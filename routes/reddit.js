var express = require("express");
const cheerio = require("cheerio");
const axios = require("axios");
var puppeteer = require("puppeteer-extra");

var StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());

var router = express.Router();

const site = "https://www.reddit.com";

async function autoScroll(page) {
  await page.evaluate(async () => {
    await new Promise((resolve, reject) => {
      var totalHeight = 0;
      var distance = 100;
      var timer = setInterval(() => {
        var scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;

        if (totalHeight >= scrollHeight) {
          clearInterval(timer);
          resolve();
        }
      }, 100);
    });
  });
}

router.get("/", function (req, res, next) {
  const link = site + "/r/selfie";

  puppeteer.launch({ headless: true }).then(async (browser) => {
    const page = await browser.newPage();
    await page.goto(link);
    await autoScroll(page);
    const html = await page.content();
    const $ = cheerio.load(html);
    // res.send(html);

    // const links = $("img[alt='Post image']")
    //   .map(function (i, el) {
    //     // this === el
    //     return $(this).attr('src');
    //   })
    //   .get();

    // const links = $(`a[href^='/user/']`)
    //   .map(function (i, el) {
    //     // this === el
    //     return $(this).attr("src");
    //   })
    //   .get();

    // const usernames = $(`.rpBJOHq2PR60pnwJlUyP0>div`)
    //   .map(function (i, el) {
    //     // this === el
    //     // return $(this).find('a[href^='/user/']').attr('src');
    //     return $(this).text();
    //   })
    //   .get();

    const usernames = $('.rpBJOHq2PR60pnwJlUyP0')
      .map(function (i, el) {
        console.log('found');
        return i;
      })
      .get();

    res.send(usernames);
    await browser.close();
  });

  // axios.get(url).then(({ data: html }) => {
  //   const $ = cheerio.load(html);

  //   const posts = $(`img=[alt='Post image']`).map((i, el) =>
  //     $(this).attr("src")
  //   );

  //   res.send(posts.get().join("<br/>"));
  // });

  // res.render("index", { title: "Express" });
});

router.get("/:username", function (req, res, next) {
  const { username } = req.params;

  res.send(username);
});

module.exports = router;

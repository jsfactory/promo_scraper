var express = require("express");
const axios = require("axios");

var router = express.Router();

router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/:username", function (req, res, next) {
  const { username } = req.params;

  const url = `https://www.instagram.com/${username}/?__a=1`;

  axios
    .get(url)
    .then(({ data }) => data)
    .then((data) => {
      const { graphql } = data;
      const { user } = graphql;
      const {
        biography: about,
        edge_followed_by: followers,
        edge_follow: following,
        full_name: name,
        profile_pic_url_hd: picture,
        edge_owner_to_timeline_media,
      } = user;

      const { edges } = edge_owner_to_timeline_media;

      const photos = edges.map((e) => e.node.display_url);

      res.send(about);
    });
});

module.exports = router;

var express = require("express");
var cheerio = require("cheerio");
var admin = require("firebase-admin");
var puppeteer = require("puppeteer-extra");

var http = require("https");
var fs = require("fs");

var numeral = require("numeral");

var StealthPlugin = require("puppeteer-extra-plugin-stealth");

puppeteer.use(StealthPlugin());

var router = express.Router();

const site = "https://www.tiktok.com/";

const local_base = "http://192.241.132.50/";

const local = local_base + "tiktok/";

const usernames = [
  "@racha1231",
  "@yakupov13",
  "@mohanadalwadiya",
  "@nickibaber",
];

admin.initializeApp({
  credential: admin.credential.cert(require("./../db.json")),
  databaseURL: "https://test-project-a4ef0.firebaseio.com",
});

const ref = admin.firestore().collection("tiktok");

function picFromStyle(style) {
  return style.substring(
    style.lastIndexOf('url("') + 5,
    style.lastIndexOf(");") - 1
  );
}

router.get("/", function (req, res, next) {
  res.send(
    usernames.map((u) => `<a href="${local + u}" >${u}<a/>`).join("<br/>")
  );
});

router.get("/test", function (req, res, next) {
  res.send(numeral("21.4".toLowerCase()));
});

router.get("/:username", function (req, res, next) {
  const { username } = req.params;
  var link = site + username;

  puppeteer.launch({args: ['--no-sandbox']}).then(async (browser) => {
    try {
      const page = await browser.newPage();
      await page.goto(link);
      // await page.waitFor(5000);
      const html = await page.content();
      const $ = cheerio.load(html);

      const name = $(".share-info>.share-title").text();
      const about = $(".share-info>.share-desc").text();

      const following = numeral(
        $('[title="Following"]').text().toLowerCase()
      ).value();
      const followers = numeral(
        $('[title="Followers"]').text().toLowerCase()
      ).value();
      const likes = numeral($('[title="Likes"]').text().toLowerCase()).value();

      const avatarStyle = $(".avatar-wrapper.round").attr("style") + "";

      const avatar = picFromStyle(avatarStyle);

      const videos = $("a.video-feed-item-wrapper")
        .map(function (i, elem) {
          const thumbnailStyle =
            $(this).children(".image-card").attr("style") + "";
          const thumbnail = picFromStyle(thumbnailStyle);
          return {
            thumbnail,
            url: $(this).attr("href"),
          };
        })
        .get();

      res.send({
        name,
        avatar,
        username,
        about,
        following,
        followers,
        likes,
        videos,
      });

      // await ref.doc(username).set({
      //   name,
      //   avatar,
      //   username,
      //   about,
      //   following,
      //   followers,
      //   likes,
      //   videos,
      // });

      // res.send(
      //   videos
      //     .map(({ url }) => {
      //       localLink = url.replace(site, local);
      //       return `<a href="${localLink}" target="_blank">${localLink}</a>`;
      //     })
      //     .join("<br/>")
      // );

      // await page.screenshot({ path: "testresult.png", fullPage: true });
      // await browser.close();
    } catch (error) {
      console.warn(error);
    }
  });
});

router.get("/:username/video/:videoid", function (req, res, next) {
  const { username, videoid } = req.params;
  var link = site + username + "/video/" + videoid;
  puppeteer.launch({ headless: true }).then(async (browser) => {
    const page = await browser.newPage();
    await page.goto(link);
    // await page.waitFor(2000);
    const html = await page.content();
    const $ = cheerio.load(html);
    // res.send(html);
    const video = $("video").attr("src");

    const dir = "./public/videos/tiktok" + username;

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    const file = fs.createWriteStream(dir + "/" + videoid + ".mp4");

    http.get(video, function (response) {
      response.pipe(file);
    });

    const download = local_base + file.path.replace("./public/", "");

    // await ref.doc(username).set({
    //   username,
    // });

    res.send(download);
    await browser.close();
  });
});

module.exports = router;

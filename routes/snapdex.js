var express = require("express");
const cheerio = require("cheerio");
const axios = require("axios");

var router = express.Router();

const base_url = "https://www.snapdex.com";

const local_base = "http://localhost:3000";

const local = local_base + "/snapdex";

router.get("/", function (req, res, next) {
  res.send("snapdex");
});

router.get("/:username", function (req, res, next) {
  const { username } = req.params;

  const url = `${base_url}/${username}`;
  axios.get(url).then(({ data: html }) => {
    const $ = cheerio.load(html);

    const picture = $("img.profile-pic").attr("src");

    const snaps = $(`a[href^='/${username}/media/snaps']`)
      .map(function (i, el) {
        return $(this).children("img").first().attr("data-src");
      })
      .get();

    // const links = $(`a[rel="noopener"]`)
    //   .map(function (i, el) {
    //     return $(this).attr("href");
    //   })
    //   .get();

    const website = $(".noopener>.fa-globe").next().text();

    res.send(website);
  });
});

module.exports = router;

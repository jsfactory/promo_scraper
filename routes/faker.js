var express = require("express");
var router = express.Router();

Number.prototype.pad = function (n = 4) {
  return (new Array(n).join("0") + this).slice(-n);
};

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/picture", function (req, res, next) {
  const r = getRndInteger(1, 2147);

  const name = r.pad() + ".jpg";

  res.send("http://localhost:3000/photos/faker/girls/" + name);
});

module.exports = router;

var express = require("express");
const cheerio = require("cheerio");
const axios = require("axios");

var http = require("https");
var fs = require("fs");

const router = express.Router();

const base_url = "https://isdb.pw";

const local_base = "http://localhost:3000";

const local = local_base + "/isdb";
// pau_eche

router.get("/", function (req, res, next) {
  res.send("done!");
});

router.get("/:username", function (req, res, next) {
  const { username } = req.params;
  const url = `${base_url}/${username}/live`;

  //   puppeteer.launch({ headless: true }).then(async (browser) => {
  //     try {
  //       const page = await browser.newPage();
  //       await page.goto(url);
  //       // await page.waitFor(5000);
  //       const html = await page.content();
  //       const $ = cheerio.load(html);

  //       const picture = $(".profile_pic_border > img").attr("data-src");

  //       const lives = $(`a`)
  //         .map(function (i, el) {
  //           return base_url + $(this).attr("href");
  //         })
  //         .get().join("<br/>");

  //       res.send(lives);
  //     } catch (error) {
  //       console.warn(error);
  //     }
  //   });

  axios.get(url).then(({ data: html }) => {
    const $ = cheerio.load(html);
    const picture = $(".profile_pic_border > img").attr("data-src");

    const lives = $(`a[href^='/${username}/live/']`)
      .map(function (i, el) {
        const url = local + $(this).attr("href");
        return `<a href="${url}">${url}</a>`;
      })
      .get()
      .join("<br/>");

    res.send(lives);
    // res.send("<img src='" + picture + "'/>");
  });
});

router.get("/:username/live/:id", function (req, res, next) {
  const { username, id } = req.params;
  const url = `${base_url}/${username}/live/${id}`;

  axios.get(url).then(({ data: html }) => {
    const $ = cheerio.load(html);

    const video = $("video>source").attr("src");
    const dir = "./public/videos/isdb/@" + username;

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    const file = fs.createWriteStream(dir + "/" + id + ".mp4");

    http.get(video, function (response) {
      response.pipe(file);
    });

    const download = local_base + file.path.replace("./public", "");
    res.send(download);
  });
});

module.exports = router;
